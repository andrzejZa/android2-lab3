package pl.edu.pwr.wiz.wzorlaboratorium3;

import android.preference.EditTextPreference;
import android.preference.Preference;
import android.text.InputType;
import android.util.Patterns;
import android.widget.EditText;

public class Validator {
    public static boolean Check(Preference pref, Object value){
       return ValidationRule.Get( pref)
               .Validate(pref,value);
    }
}
class ValidationRule{

    static IValidationRule Get(Preference pref){
        if (pref instanceof CustomEditTextPreference) {
         switch (((CustomEditTextPreference) pref).getValidationRule()){
             default:
                 return new DefaultValidationRule();

         }
        }
        return new DefaultValidationRule();
    }

}

interface IValidationRule {
    public boolean Validate(Preference pref, Object value);
}
class DefaultValidationRule implements IValidationRule{


    @Override
    public boolean Validate(Preference pref, Object value) {
        if (pref instanceof EditTextPreference) {
            EditText editText = ((EditTextPreference)pref).getEditText();
            String text = value.toString();
            if (text.length()==0)
                return true;
            switch (editText.getInputType()){
                case InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD:
                    if(text.indexOf(' ')>-1)
                        return false;
                    return true;

                case InputType.TYPE_CLASS_TEXT |InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS:

                    return android.util.Patterns.EMAIL_ADDRESS.matcher(text).matches() && text.length()>0;

                case InputType.TYPE_CLASS_TEXT |InputType.TYPE_TEXT_VARIATION_URI:
                    return Patterns.WEB_URL.matcher(text).matches() && text.length()>0;


                default:
                    return true;
            }


        }
        return true;
    }
}

