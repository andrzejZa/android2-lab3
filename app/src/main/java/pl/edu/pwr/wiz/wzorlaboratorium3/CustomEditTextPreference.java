package pl.edu.pwr.wiz.wzorlaboratorium3;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.util.AttributeSet;
import android.view.View;

public class CustomEditTextPreference extends EditTextPreference{

    private String validationRule;

    public CustomEditTextPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        validationRule = getValidationRule(context,attrs);

    }

    public CustomEditTextPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        validationRule = getValidationRule(context,attrs);

    }

    public CustomEditTextPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
       validationRule = getValidationRule(context,attrs);
    }

    public CustomEditTextPreference(Context context) {
        super(context);
    }

    private  String getValidationRule(Context context, AttributeSet attrs){
        TypedArray a = context.obtainStyledAttributes(attrs,R.styleable.validation);
        CharSequence s = a.getString(R.styleable.validation_validation_rule);
        a.recycle();
        return  s==null?"":s.toString();
    }

    @Override
    protected void showDialog(Bundle state) {
        super.showDialog(state);
        AlertDialog dlg = (AlertDialog) getDialog();
        View positiveButton = dlg.getButton(DialogInterface.BUTTON_POSITIVE);
        getEditText().setError(null);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPositiveButtonClicked(v);
            }
        });
    }

    private void onPositiveButtonClicked(View v) {
        String errorMessage = onValidate(getEditText().getText().toString());
        if (errorMessage == null) {
            getEditText().setError(null);
            onClick(getDialog(), DialogInterface.BUTTON_POSITIVE);
            getDialog().dismiss();
        } else {
            getEditText().setError(errorMessage);
            return; // return WITHOUT dismissing the dialog.
        }
    }

    /***
     * Called to validate contents of the edit text.
     *
     * Return null to indicate success, or return a validation error message to display on the edit text.
     *
     * @param text The text to validate.
     * @return An error message, or null if the value passes validation.
     */
    public String onValidate(String text) {
        boolean isValid = Validator.Check(this,text);
        if (isValid)
            return null;
        return "invalid input";
    }

    public String getValidationRule(){
        return validationRule;
    }
}