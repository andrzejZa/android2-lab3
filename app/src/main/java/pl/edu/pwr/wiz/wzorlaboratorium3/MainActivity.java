package pl.edu.pwr.wiz.wzorlaboratorium3;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /* Resetowanie ustawień */
        Button btn = (Button) findViewById(R.id.btn_reset);
        final Button twitterBtn = (Button) findViewById(R.id.twitter_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(view.getContext());

                // Tytul
                alertDialogBuilder.setTitle("Reset ustawień");

                // Ustawiamy dialog
                alertDialogBuilder
                        .setMessage("Kliknij tak, aby potwierdzić?")
                        .setCancelable(false)
                        .setPositiveButton("Tak",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // Kliknięto tak
                                //  Reset ustawień
                                pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                pref.edit().clear().apply();
                                MainActivity.this.recreate();
                                //  Wyświetlenie Toast z informacją
                                Toast.makeText(MainActivity.this, "Ustawienia zostaly zresetowane",
                                        Toast.LENGTH_LONG).show();
                            }
                        })
                        .setNegativeButton("Nie",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // Kliknięto nie
                                //  Wyświetlenie Toast z informacją
                                Toast.makeText(MainActivity.this, Html.fromHtml("Ustawienia <b>NIE</b> zostaly zresetowane"),
                                        Toast.LENGTH_LONG).show();
                                dialog.cancel();
                            }
                        });

                // Utworz okno dialogowe i wyswietl je
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });


        twitterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = twitterBtn.getText().toString();
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        showPrefs();
    }

    private void showPrefs() {
        /* Pobieramy ustawienia i wyswietlamy login FB, jesli jest ustawiony */
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        Boolean fb_on = pref.getBoolean("facebook_on", false);
        TextView textView = (TextView) findViewById(R.id.dane_fb);

        if(fb_on) {
            textView.setText("Twój login FB to: " + pref.getString("facebook_login", "brak"));
        } else {
            textView.setText("FB wyłączone");
        }

        // @TODO Wyświetlanie przycisku kierującego do profilu Twitter, o ile taki został ustawiony
        boolean twitter_on = pref.getBoolean("twitter_on",false);
        Button twitterBtn = (Button) findViewById(R.id.twitter_btn);
        if (twitter_on){
            twitterBtn.setVisibility(View.VISIBLE);

            String twitterUrl = pref.getString("twitter_url","");
            twitterBtn.setText(twitterUrl);

        } else {
            twitterBtn.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent settings = new Intent(this, SettingsActivity.class);
            startActivity(settings);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

